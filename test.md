## MENORCA
Bonica illa enmig del mediterràni, molt rica i plena de paisatges.
Molta varietat d'activitats disponibles.

### Pobles Ordenats
1. Ciutadella
2. Ferreries
3. Es Mercadal

### Pobles desordenats
- Maó
- Es Migorn Gran
- Alaior

[camí de cavalls](http://www.camidecavalls.com/)

![Menorca](menorca.jpg)
